# Autobusarenda

Autobusarenda website https://autobusarenda.ru/

## Configuration
- PHP 7.4
- MySql 5.7
- Nginx 1.13
- Nodejs 11.15

## Install

Copy configuration file:
```bash
cp .env.example .env
```

Add env `FRONTEND_HOST` and `BACKEND_HOST` variables values to `/etc/hosts` file:
```
127.0.1.1	autobusarenda.docker
127.0.1.1	admin.autobusarenda.docker
```

Build docker containers:
```bash
make build
```

Start docker containers:
```bash
make start
```

## Import local db dump
Copy `dump.sql` file to root folder.

Import dump:
```bash
cat dump.sql | docker exec -i autobusarenda-mysql mysql -uroot -proot autobusarenda
```

Use `FRONTEND_HOST` variable value: http://mobili-stile.docker  
Use `BACKEND_HOST` variable value: http://admin.autobusarenda.docker

Creates symlinks:
- `storage/public` -> `app/backend/web/storage`
- `storage/public` ->  `app/frontend/web/storage`

```bash
docker exec -i autobusarenda-php bash -c "php yii yiicom/storage/create-public-links"
```

- `storage/temp` -> `app/backend/web/temp`
```bash
docker exec -i autobusarenda-php bash -c "php yii yiicom/storage/create-temp-link"
```

## Frontend
Frontend webpack app:
```bash 
make frontend-watch
make frontend-build
```

Backend webpack app:
```bash 
make backend-watch
make backend-build
```

## Deploy
Using PHP Deployer 6.6.0

Copy configuration file:
```bash
cp deploy.yml.example deploy.yml
```

Deploy to `prod` environment:
```bash
dep deploy prod
```

After deployment, you need to reset opcache on the prod server:
```bash
sudo service php7.4-fpm restart
```


## Image optimization
Run script from images folder:

```bash
sh ~/www/autobusarenda/scripts/jpeg_optimize.sh
```

## Cronjob
```text
* * * * * php /home/webuser/www/autobusarenda/current/yii schedule/run --scheduleFile=@console/config/schedule.php 1>> /dev/null 2>&1
```

## Yii commerce modules
- yiicom
- yiicom-content
- yiicom-categories
- yiicom-files


## TODO:
    
breads widget
  
- yiicom-files
    - Images presets action help (resize, in, out)
    - Image delete in form
    - Удаление изображений с основным матералом
    
- yiicom-content
    - page template field move to url table
    - url alias params and sitemap edit
    - [CKEDITOR] Error code: editor-destroy-iframe.
    - articles, news, etc templates
    
- yiicom/catalog
    - remove category with children    
    
- Title auto fill from name field by default    
- Bring vue component names to a single form  (see catalog module)  
- Move translate in modules
- Console translate
- gridview boolean column
- AttributeList to collection class
- Url prefix for producs, pages categories