#!/usr/bin/env bash

set -eo pipefail

composer install --no-interaction --no-scripts --no-progress

php-fpm -F -O
