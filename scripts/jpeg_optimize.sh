#!/usr/bin/env sh

# To use run in images folder:
# sh ~/www/autobusarenda/scripts/jpeg_optimize.sh

resultDir="./optimized";

mkdir -p ${resultDir}

for fileName in *.jpg
do
    cjpeg -optimize -progressive -quality 90 ${fileName} > ${resultDir}/${fileName}
    jpegoptim --strip-all ${fileName}
done