#!/bin/bash

#. ./../.env

export $(grep -v '^#' .env | xargs)

date=`date +%Y%m%d-%H%M%S`
filename="app/console/runtime/dump_${date}.sql.gz"

echo "SSH host name: "$PROD_HOST" try dumping..."
ssh $PROD_HOST "mysqldump -u$PROD_DB_USER -p$PROD_DB_PASSWORD $PROD_DB_DATABASE | gzip" > ${filename}


if [ ! -f "$filename" ] || [ ! `du -b $filename | cut -f1` -ge 100 ] ;then
    echo "ERROR! Can't create dump!"
    exit 1
fi

echo "OK"
echo "Dump file: $filename"
echo -n "Try upload to Local (y/n)?"
read answer

if [ "$answer" != "${answer#[Yy]}" ] ;then
    echo "Uploading to local..."
    zcat ${filename} | docker-compose exec -T mysql mysql -u$DB_USER -p$DB_PASSWORD ${DB_DATABASE}
    echo "OK"
    exit 0
else
    exit 0
fi
