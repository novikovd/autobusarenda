DOCKER_COMPOSE = docker compose
SHELL := /bin/bash
DEFAULT_GOAL := help
THIS_FILE := $(lastword $(MAKEFILE_LIST))

help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-27s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-27s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

.PHONY: start
start: ## Start application
	@echo -e "\033[33mstart\033[0m"
	@USERID=$$(id -u) GROUPID=$$(id -g) $(DOCKER_COMPOSE) --file docker-compose.yml up

.PHONY: build
build: ## Build application
	@echo -e "\033[33mstart\033[0m"
	@docker build .docker/php -f .docker/php/dockerfile --tag autobusarenda-php-base

	@USERID=$$(id -u) GROUPID=$$(id -g) $(DOCKER_COMPOSE) --file docker-compose.yml build

.PHONY: rebuild
rebuild: ## Build application from scratch, without cache etc
	@echo -e "\033[33mrebuild\033[0m"
	@USERID=$$(id -u) GROUPID=$$(id -g) $(DOCKER_COMPOSE) --file docker-compose.yml rm -sf
	@USERID=$$(id -u) GROUPID=$$(id -g) $(DOCKER_COMPOSE) --file docker-compose.yml build --no-cache --progress=plain
	@USERID=$$(id -u) GROUPID=$$(id -g) $(DOCKER_COMPOSE) --file docker-compose.yml up --force-recreate --build --no-deps

.PHONY: clean
clean: ## Clean containers and application cache
	@echo -e "\033[33mclean\033[0m"
	@echo -e "\033[33mclean-docker\033[0m"
	@USERID=$$(id -u) GROUPID=$$(id -g) $(DOCKER_COMPOSE) --file docker-compose.yml down --volumes --remove-orphans --rmi local
	@if [ "$$(docker images -q -f dangling=true)" != "" ]; then docker rmi -f $$(docker images -q -f dangling=true) ;fi

	@rm -rf "./app/vendor"
	@rm -rf "./app/runtime/cache"
	@rm -rf "./app/runtime/debug"
	@rm -rf "./app/runtime/log"

.PHONY: frontend-watch
frontend-watch: ## Run frontend webpack in dev mode
	@echo -e "\033[33mfrontend-watch\033[0m"
	@docker run --rm \
				-t \
				--volume $(HOME)/.cache:/home/docker/.cache:delegated \
				--volume $(HOME)/.npm:/home/docker/.npm:delegated \
				--volume $(PWD):/app:delegated \
				--env USERID=$$(id -u) \
				--env GROUPID=$$(id -g) \
				--name autobusarenda-node-runtime \
				autobusarenda-node \
				bash -c "npm run-script frontend:watch"

.PHONY: frontend-build
frontend-build: ## Run frontend webpack in prod mode
	@echo -e "\033[33mfrontend-build\033[0m"
	@docker run --rm \
	            -t \
	            --volume $(HOME)/.cache:/home/docker/.cache:delegated \
				--volume $(HOME)/.npm:/home/docker/.npm:delegated \
				--volume $(PWD):/app:delegated \
				--env USERID=$$(id -u) \
				--env GROUPID=$$(id -g) \
				--name autobusarenda-node-runtime \
				autobusarenda-node \
				bash -c "npm run-script frontend:build"

.PHONY: backend-watch
backend-watch: ## Run backend webpack in dev mode
	@echo -e "\033[33mbackend-watch\033[0m"
	@docker run --rm \
				-t \
				--volume $(HOME)/.cache:/home/docker/.cache:delegated \
				--volume $(HOME)/.npm:/home/docker/.npm:delegated \
				--volume $(PWD):/app:delegated \
				--env USERID=$$(id -u) \
				--env GROUPID=$$(id -g) \
				--name autobusarenda-node-runtime \
				autobusarenda-node \
				bash -c "npm run-script backend:watch"

.PHONY: backend-build
backend-build: ## Run backend webpack in prod mode
	@echo -e "\033[33mbackend-build\033[0m"
	@docker run --rm \
	            -t \
	            --volume $(HOME)/.cache:/home/docker/.cache:delegated \
				--volume $(HOME)/.npm:/home/docker/.npm:delegated \
				--volume $(PWD):/app:delegated \
				--env USERID=$$(id -u) \
				--env GROUPID=$$(id -g) \
				--name autobusarenda-node-runtime \
				autobusarenda-node \
				bash -c "npm run-script backend:build"