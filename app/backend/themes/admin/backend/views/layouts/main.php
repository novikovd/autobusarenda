<?php

use yii\web\View;
use yii\helpers\Html;
use backend\assets\AppAsset;

/**
 * @var View $this
 * @var string $content
 */

$this->registerAssetBundle(AppAsset::class);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?php echo \Yii::$app->language; ?>">
<head>
    <meta charset="<?php echo \Yii::$app->charset; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php echo Html::csrfMetaTags(); ?>
    <title><?php echo Html::encode($this->title); ?></title>
    <?php echo $this->head(); ?>
</head>
<body>
<?php $this->beginBody(); ?>

    <div id="app"></div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
