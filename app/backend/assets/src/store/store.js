import Vue from 'vue';
import Vuex from 'vuex';

import commerce from '../../../../vendor/yiicom/yiicom/src/backend/assets/src/store/commerce.js';
import contentPages from '../../../../vendor/yiicom/yiicom-content/src/backend/assets/src/store/pages.js';
import contentCategories from '../../../../vendor/yiicom/yiicom-content/src/backend/assets/src/store/categories.js'
import contentRelation from '../../../../vendor/yiicom/yiicom-content/src/backend/assets/src/store/relation.js'
import contentRelationGroup from '../../../../vendor/yiicom/yiicom-content/src/backend/assets/src/store/relationGroup.js'
import filesPresets from '../../../../vendor/yiicom/yiicom-files/src/backend/assets/src/store/presets.js'
import catalogCategory from '../../../../vendor/yiicom/yiicom-catalog/src/backend/assets/src/store/category.js'
import catalogProduct from '../../../../vendor/yiicom/yiicom-catalog/src/backend/assets/src/store/product.js'
import catalogAttribute from '../../../../vendor/yiicom/yiicom-catalog/src/backend/assets/src/store/attribute.js'
import catalogAttributeGroup from '../../../../vendor/yiicom/yiicom-catalog/src/backend/assets/src/store/attributeGroup.js'

Vue.use(Vuex);

export default new Vuex.Store({

    namespaced: true,

    modules: {
        'commerce': commerce,
        'content-pages': contentPages,
        'content-categories': contentCategories,
        'content-relation': contentRelation,
        'content-relation-group': contentRelationGroup,
        'files-presets': filesPresets,
        'catalog-category': catalogCategory,
        'catalog-product': catalogProduct,
        'catalog-attribute': catalogAttribute,
        'catalog-attribute-group': catalogAttributeGroup
    }

});
