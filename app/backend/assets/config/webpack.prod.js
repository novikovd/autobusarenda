const merge = require('webpack-merge');
const main = require('./../../../common/assets/config/webpack');
const frontend = require('./webpack');

const config = merge(main, frontend, {
    mode: 'production',
    // optimization: {
    //     minimize: false,
    // },
});

module.exports = new Promise((resolve, reject) => {
    resolve(config);
});