<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\YiiAsset;
use yii\bootstrap4\BootstrapAsset;
use yii\bootstrap4\BootstrapPluginAsset;
use rmrevin\yii\fontawesome\AssetBundle as FontawesomeAsset;
use yiicom\backend\assets\ckeditor\CKEditorAsset;

/**
 * @inheritdoc
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/dist';

    public $css = [
		'css/app.css',
    ];

    public $js = [
        'js/vendors.js',
		'js/app.js',
    ];

    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
        FontawesomeAsset::class,
        CKEditorAsset::class,
    ];
}
