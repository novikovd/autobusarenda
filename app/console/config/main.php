<?php

return [
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => dmstr\console\controllers\MigrateController::class,
            'migrationTable' => '{{%migrations}}',
            'migrationPath' => '@migrations',
        ],
    ],
//    'components' => [
//        'schedule' => [
//            'class' => \marketingsolutions\scheduling\Schedule::class,
//        ],
//    ],
    'modules' => [
        'yiicom' => [
            'class' => yiicom\console\Module::class,
        ],
        'content' => [
            'class' => yiicom\content\console\Module::class
        ],
        'catalog' => [
            'class' => yiicom\catalog\console\Module::class
        ],
        'files' => [
            'class' => yiicom\files\console\Module::class
        ],
    ],
    'params' => [
        'yii.migrations' => [
            // TODO: move to modules Bootstrap.php
            '@yiicom/migrations',
            '@yiicom/content/migrations',
            '@yiicom/files/migrations',
            '@yiicom/catalog/migrations',

        ],
    ],
];