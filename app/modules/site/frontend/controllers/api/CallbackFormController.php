<?php

namespace modules\site\frontend\controllers\api;

use Yii;
use yii\rest\Controller;
use modules\site\frontend\forms\CallbackForm;

class CallbackFormController extends Controller
{
    public function actionGet()
    {
        return [
            'title' => 'Обратный звонок',
            'body' => $this->renderPartial('form'),
        ];
    }

    public function actionSet()
    {
        $model = new CallbackForm();
        $model->load(Yii::$app->request->post(), '');
        $model->process();

        return $model;
    }
}
