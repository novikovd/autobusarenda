<?php

namespace modules\site\frontend\controllers\api;

use Yii;
use yii\rest\Controller;
use modules\site\frontend\forms\OrderForm;

class OrderFormController extends Controller
{
    public function actionSet()
    {
        $model = new OrderForm();
        $model->load(Yii::$app->request->post(), '');
        $model->process();

        return $model;
    }
}
