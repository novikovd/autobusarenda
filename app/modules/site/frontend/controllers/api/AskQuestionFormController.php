<?php

namespace modules\site\frontend\controllers\api;

use Yii;
use yii\rest\Controller;
use modules\site\frontend\forms\AskQuestionForm;

class AskQuestionFormController extends Controller
{
    public function actionSet()
    {
        $model = new AskQuestionForm();
        $model->load(Yii::$app->request->post(), '');
        $model->process();

        return $model;
    }
}
