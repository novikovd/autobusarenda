<?php

namespace modules\site\frontend\controllers\api;

use Yii;
use yii\rest\Controller;
use modules\site\frontend\forms\ReviewForm;

class ReviewFormController extends Controller
{
    public function actionSet()
    {
        $model = new ReviewForm();
        $model->load(Yii::$app->request->post(), '');
        $model->process();

        return $model;
    }
}
