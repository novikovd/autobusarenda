<?php

namespace modules\site\frontend\controllers;

use Yii;
use yiicom\common\base\Controller;
use yiicom\content\common\models\Page;
use yiicom\content\frontend\traits\SitePageTrait;
use yiicom\catalog\common\models\Product;
use yiicom\catalog\common\models\Category;

class PageController extends Controller
{
    use SitePageTrait;

    const CATEGORY_BUS_ID = 21;
    const CATEGORY_MICRO_BUS_ID = 22;
    const CATEGORY_CAR_ID = 20;

    /**
     * All prices page (url: /ceny)
     * @param $id
     * @return string
     */
    public function actionPrices($id)
    {
        /* @var Page $page */
		$page = $this->loadModel(Page::class, $id);

		$categoryIds = [self::CATEGORY_BUS_ID, self::CATEGORY_MICRO_BUS_ID, self::CATEGORY_CAR_ID];
        $categories = [];
        $products = [];

		foreach ($categoryIds as $categoryId) {
            $categories[] = Category::find()
                ->withUrl()
                ->id($categoryId)
                ->one();

            $products[$categoryId] = Product::find()
                ->withUrl()
                ->category([$categoryId])
                ->active()
                ->orderBy(['price' => SORT_ASC])
                ->all();
        }

		return $this->render($this->getTemplate($page), [
		    'page' => $page,
		    'categories' => $categories,
            'products' => $products,
        ]);
    }

    /**
     * Make order page (url: /arendovat)
     * @param int $id
     * @return string
     */
    public function actionOrder($id)
    {
        /* @var Page $page */
        $page = $this->loadModel(Page::class, $id);
        $productId = (int) Yii::$app->request->get('productId');

        $product = Product::find()
            ->withUrl()
            ->where([Product::tableName().'.id' => $productId])
            ->one();

        return $this->render($this->getTemplate($page), [
            'page' => $page,
            'product' => $product,
        ]);
    }
}
