<?php

namespace modules\site\frontend\forms;

use Yii;
use yii\base\Model;
use yiicom\common\schedule\SendMailTask;
use yiicom\common\schedule\Scheduler;
use yiicom\common\validators\PhoneValidator;

class ReviewForm extends Model
{
    /** @var string */
    public $name;

    /** @var string */
    public $phone;

    /** @var string */
    public $email;

    /** @var string */
    public $title;

    /** @var string */
    public $company;

    /** @var integer */
    public $rating;

    /** @var string */
    public $comment;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required', 'message' => 'Укажите ваше имя'],

            ['phone', 'filter', 'filter' => 'trim'],
            ['phone', 'required', 'message' => 'Укажите телефон или email', 'when' => function(ReviewForm $model) {
                return $model->email == '';
            }],
            ['phone', PhoneValidator::class],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => 'Укажите email или телефон', 'when' => function(ReviewForm $model) {
                return $model->phone == '';
            }],
            ['email', 'email', 'message' => 'Укажите корректный email'],

            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'string', 'max' => 255],

            ['company', 'filter', 'filter' => 'trim'],
            ['company', 'string', 'max' => 255],

            ['rating', 'required', 'message' => 'Выберите оценку'],
            ['rating', 'in', 'range' => range(1, 5)],

            ['comment', 'filter', 'filter' => 'trim'],
            ['comment', 'required', 'message' => 'Укажите Ваш отзыв'],
            ['comment', 'string', 'max' => 2048],
        ];
    }

    public function process()
    {
        if (! $this->validate()) {
            return false;
        }

        $to = Yii::$app->params['email']['primary'];
        $subject = 'Оставить отзыв';
        $body = Yii::$app->view->renderFile('@frontend/themes/site/mail/review-form/admin.php', [
            'subject' => $subject,
            'form' => $this,
        ]);

        $task = new SendMailTask($to, $subject, $body);
        $scheduler = new Scheduler();

        return $scheduler->createSchedule($task, $this)->save();
    }

}