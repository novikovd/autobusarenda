<?php

namespace modules\site\frontend\forms;

use Yii;
use yii\base\Model;
use yiicom\common\schedule\SendMailTask;
use yiicom\common\schedule\Scheduler;
use yiicom\common\validators\PhoneValidator;
use yiicom\catalog\common\models\Product;

class OrderForm extends Model
{
    const TYPE_RETURN = 1;
    const TYPE_TRANSFER = 2;

    /** @var string */
    public $name;

    /** @var string */
    public $phone;

    /** @var string */
    public $email;

    /** @var string */
    public $comment;

    /** @var integer */
    public $type;

    /** @var string */
    public $start;

    /** @var string */
    public $end;

    /** @var string */
    public $from;

    /** @var string */
    public $to;

    /** @var integer */
    public $productId;

    /**
     * @return array
     */
    public function types()
    {
        return [
            self::TYPE_RETURN => 'Туда и обратно',
            self::TYPE_TRANSFER => 'Трансфер',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required', 'message' => 'Укажите ваше имя'],

            ['phone', 'filter', 'filter' => 'trim'],
            ['phone', 'required', 'message' => 'Укажите телефон или email', 'when' => function(OrderForm $model) {
                return $model->email == '';
            }],
            ['phone', PhoneValidator::class],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => 'Укажите email или телефон', 'when' => function(OrderForm $model) {
                return $model->phone == '';
            }],
            ['email', 'email', 'message' => 'Укажите корректный email'],

            ['comment', 'safe'],

            ['type', 'in', 'range' => [self::TYPE_RETURN, self::TYPE_TRANSFER]],

            ['start', 'filter', 'filter' => 'trim'],
            ['start', 'string', 'max' => 255],

            ['end', 'filter', 'filter' => 'trim'],
            ['end', 'string', 'max' => 255],

            ['from', 'filter', 'filter' => 'trim'],
            ['from', 'string', 'max' => 255],

            ['to', 'filter', 'filter' => 'trim'],
            ['to', 'string', 'max' => 255],

            ['productId', 'integer'],
        ];
    }

    public function process()
    {
        if (! $this->validate()) {
            return false;
        }

        $product = Product::find()
            ->withUrl()
            ->where([Product::tableName().'.id' => $this->productId])
            ->one();

        $to = Yii::$app->params['email']['primary'];
        $subject = 'Заказ';
        $body = Yii::$app->view->renderFile('@frontend/themes/site/mail/order-form/admin.php', [
            'subject' => $subject,
            'form' => $this,
            'product' => $product,
        ]);

        $task = new SendMailTask($to, $subject, $body);
        $scheduler = new Scheduler();

        return $scheduler->createSchedule($task, $this)->save();
    }

}