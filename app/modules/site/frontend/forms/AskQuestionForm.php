<?php

namespace modules\site\frontend\forms;

use Yii;
use yii\base\Model;
use yiicom\common\schedule\SendMailTask;
use yiicom\common\schedule\Scheduler;
use yiicom\common\validators\PhoneValidator;

class AskQuestionForm extends Model
{
    /** @var string */
    public $name;

    /** @var string */
    public $phone;

    /** @var string */
    public $email;

    /** @var string */
    public $category;

    /** @var string */
    public $question;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required', 'message' => 'Укажите ваше имя'],

            ['phone', 'filter', 'filter' => 'trim'],
            ['phone', 'required', 'message' => 'Укажите телефон или email', 'when' => function(AskQuestionForm $model) {
                return $model->email == '';
            }],
            ['phone', PhoneValidator::class],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => 'Укажите email или телефон', 'when' => function(AskQuestionForm $model) {
                return $model->phone == '';
            }],
            ['email', 'email', 'message' => 'Укажите корректный email'],

            ['category', 'filter', 'filter' => 'trim'],
            ['category', 'string', 'max' => 255],

            ['question', 'filter', 'filter' => 'trim'],
            ['question', 'required', 'message' => 'Задайте вопрос'],
            ['question', 'string', 'max' => 2048],
        ];
    }

    public function process()
    {
        if (! $this->validate()) {
            return false;
        }

        $to = Yii::$app->params['email']['primary'];
        $subject = 'Задать вопрос';
        $body = Yii::$app->view->renderFile('@frontend/themes/site/mail/ask-question-form/admin.php', [
            'subject' => $subject,
            'form' => $this,
        ]);

        $task = new SendMailTask($to, $subject, $body);
        $scheduler = new Scheduler();

        return $scheduler->createSchedule($task, $this)->save();
    }

}