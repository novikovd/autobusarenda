<?php

namespace modules\site\frontend\forms;

use Yii;
use yii\base\Model;
use yiicom\common\schedule\SendMailTask;
use yiicom\common\schedule\Scheduler;
use yiicom\common\validators\PhoneValidator;

class CallbackForm extends Model
{
    /** @var string */
    public $name;

    /** @var string */
    public $phone;

    /** @var string */
    public $comment;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required', 'message' => 'Укажите ваше имя'],

            ['phone', 'filter', 'filter' => 'trim'],
            ['phone', 'required', 'message' => 'Укажите номер телефона'],
            ['phone', PhoneValidator::class],

            ['comment', 'safe'],
        ];
    }

    public function process()
    {
        if (! $this->validate()) {
            return false;
        }

        $to = Yii::$app->params['email']['primary'];
        $subject = 'Обратный звонок';
        $body = Yii::$app->view->renderFile('@frontend/themes/site/mail/callback-form/admin.php', [
            'subject' => $subject,
            'form' => $this
        ]);

        $task = new SendMailTask($to, $subject, $body);
        $scheduler = new Scheduler();

        return $scheduler->createSchedule($task, $this)->save();
    }

}