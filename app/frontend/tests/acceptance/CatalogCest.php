<?php

namespace frontend\tests\acceptance;

use frontend\tests\AcceptanceTester;

class CatalogCest
{
    public function checkCatalogPage(AcceptanceTester $I)
    {
        $I->amOnPage('/katalog-uslug');

        $I->see('КАТАЛОГ УСЛУГ');

        $I->seeLink('Аренда автобусов', '/katalog-uslug/arenda-avtobusov');
        $I->seeLink('Аренда микроавтобусов', '/katalog-uslug/arenda-mikroavtobusov');
        $I->seeLink('Аренда легковых автомобилей', '/katalog-uslug/arenda-legkovyh-avtomobilej');
        $I->seeLink('Туристические автобусы', '/katalog-uslug/turisticheskie-avtobusy');
        $I->seeLink('Трансфер', '/katalog-uslug/transfer');
        $I->seeLink('Корпоративные перевозки', '/katalog-uslug/korporativnye-perevozki');
        $I->seeLink('Автобусы на свадьбу', '/katalog-uslug/avtobusy-na-svadbu');
        $I->seeLink('Автобусы для детей', '/katalog-uslug/avtobusy-dlja-detej');
    }

}
