<?php

namespace frontend\tests\acceptance;

use frontend\tests\AcceptanceTester;

class IndexCest
{
    public function checkIndexPage(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('НАШИ УСЛУГИ');
        $I->see('АРЕНДА АВТОБУСА С ВОДИТЕЛЕМ');
        $I->see('ПОЧЕМУ ВЫБИРАЮТ НАС');
        $I->see('КАК ЗАКАЗАТЬ');
        $I->see('ВЫСОКОКАЧЕСТВЕННЫЕ ПАССАЖИРСКИЕ ПЕРЕВОЗКИ');

        $I->seeLink('Услуги и транспорт', '/katalog-uslug');
    }

}
