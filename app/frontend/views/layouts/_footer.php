<?php

use yiicom\common\base\View;
use yiicom\common\helpers\SvgIcon;
use yiicom\common\helpers\StringHelper;

/**
 * @var View $this
 * @var array $params
 */

?>

<div class="footer">
    <div class="container">

        <div class="row footer__wrap">
            <div class="col-lg-4 col-md-4 col-sm-6 col-12">

                <a class="footer__phone" href="tel:+<?= StringHelper::toNumber(Yii::$app->params['phone']['primary']); ?>" title="Позвонить по телефону">
                    <?= $params['phone']['primary'] ?>
                </a><br>
                <?php if ($params['phone']['secondary']) : ?>
                    <a class="footer__phone" href="tel:+<?= StringHelper::toNumber(Yii::$app->params['phone']['secondary']); ?>" title="Позвонить по телефону">
                        <?= $params['phone']['secondary'] ?>
                    </a><br>
                <?php endif; ?>
                <a class="link footer__callback mb-4" href="#" data-toggle="remote-modal" data-target="/site/api/callback-form/get">Заказать звонок</a><br>

                <div class="mb-4">
                    <p class="footer__title">Написать нам</p>
                    <a class="link" href="/arendovat">Рассчитать стоимость</a><br>
                    <a class="link" href="/zadat-vopros">Задать вопрос</a><br>
                    <a class="link" href="/ostavit-otzyv">Оставить отзыв</a><br>
                    <a class="link" href="mailto:<?= $params['email']['primary'] ?>"><?= $params['email']['primary'] ?></a>
                </div>

                <div class="mb-4">
                    <p class="footer__title"><a href="/kontakty" title="Подробные контакты и схема проезда">Контакты</a></p>
                    <a class="link-light footer__address" href="/kontakty"><?= $params['address']['middle'] ?></a>
                    <p class="footer__company"><?= $params['company']['title'] ?> ИНН <?= $params['company']['inn'] ?></p>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                <div class="menu menu-footer">
                    <a class="menu__title" href="/katalog-uslug">Наши Услуги</a>
                    <ul class="menu__list">
                        <li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/arenda-avtobusov" title="Аренда автобуса с водителем">Аренда автобуса</a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/arenda-mikroavtobusov" title="Аренда микроавтобуса или минивэна с водителем">Аренда микроавтобуса</a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/arenda-legkovyh-avtomobilej" title="Аренда автомобиля с водителем">Аренда автомобиля</a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/turisticheskie-avtobusy" title="Заказать туристический автобус">Туристические автобусы</a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/transfer" title="Заказать автобус трансфер">Трансфер</a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/avtobus-v-ajeroport" title="Трансфер в аэропорт">Автобус в аэропорт</a>
                        </li><li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/transfery-na-vokzal" title="Трансферы на вокзал">Автобус на вокзал</a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/mezhdugorodnie-avtobusy" title="Заказать междугородние автобусы">Междугородние автобусы</a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/avtobusy-na-korporativ" title="Заказать автобус на корпоратив">Автобусы на корпоратив</a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/perevozka-sotrudnikov" title="Перевозка сотрудников">Перевозка сотрудников</a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/avtobusy-na-svadbu" title="Заказать автобусы на свадьбу">Автобусы на свадьбу</a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/avtobusy-dlja-detej" title="Заказать автобус автобусы для детей и школьников">Автобусы для детей</a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/arenda-avtobusa-dlja-jekskursij" title="Аренда детских экскурсионных автобусов">Аренда автобуса для экскурсий</a>
                        </li>
                        <li class="menu__item">
                            <a class="menu__link" href="/katalog-uslug/avtobus-na-vypusknoj" title="Заказать автобусы на выпускной">Автобусы на выпускной</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                <div class="menu menu-footer">
                    <a class="menu__title" href="/klientam" title="Byajhvfwbz lkz r">Клиентам</a>
                    <ul class="menu__list">
                        <li class="menu__item"><a class="menu__link" href="/klientam/kak-zakazat">Как заказать</a></li>
                        <li class="menu__item"><a class="menu__link" href="/klientam/pravila-arendy">Правила аренды</a></li>
                        <li class="menu__item"><a class="menu__link" href="/klientam/voprosy-i-otvety">Вопросы и ответы</a></li>
                        <li class="menu__item"><a class="menu__link" href="/klientam/pravila-organizovannoj-perevozki-detej">Правила перевозки детей</a></li>
                        <li class="menu__item"><a class="menu__link" href="/klientam/konfidencialnost">Конфиденциальность</a></li>
                    </ul>
                </div>

                <div class="menu menu-footer">
                    <a class="menu__title" href="/o-nas">О нас</a>
                    <ul class="menu__list">
                        <li class="menu__item"><a class="menu__link" href="/o-nas/o-kompanii">О компании</a></li>
                        <li class="menu__item"><a class="menu__link" href="/o-nas/dokumenty">Документы</a></li>
                        <li class="menu__item"><a class="menu__link" href="/novosti">Новости</a></li>
                        <li class="menu__item"><a class="menu__link" href="/stati">Статьи</a></li>
                    </ul>

                    <p class="footer__title">Мы в соцсетях</p>
                    <div class="footer__social">
                        <a href="https://www.instagram.com/autobusarenda/" title="Мы в Instagram" target="_blank">
                            <?= new SvgIcon('instagram', 'md') ?>
                        </a>
                        <a href="https://facebook.com/autobusarenda/" title="Мы в Facebook" target="_blank">
                            <?= new SvgIcon('facebook', 'md') ?>
                        </a>
                    </div>
                </div>
            </div>

        </div>

    </div><!-- /.container -->

    <div class="footer__bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12">Предложение не является публичной офертой</div>
                <div class="col-md-6 col-12 text-lg-right text-md-left">© <?= date('Y') ?> «AUTOBUSARENDA.RU» - аренда автобуса c водителем<br></div>
            </div>
        </div>
    </div>
</div>


