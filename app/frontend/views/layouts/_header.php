<?php

use yiicom\common\helpers\SvgIcon;
use yiicom\common\helpers\StringHelper;

/**
 * @var array $params
 */
?>

<header class="header">

    <div class="row">

        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6">
            <a class="logo header__logo" href="/" title="Autobusarenda.ru - аренда автобусов с водителем">
                <span class="logo__name">Autobusarenda</span>
                <span class="logo__icon"></span>
                <span class="logo__title">Аренда автобусов</span>
            </a>
        </div>

        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-xs-6 col-12 header__contacts">
            <div class="header__phone">
                <a class="link-dark header__number" href="tel:+<?= StringHelper::toNumber(Yii::$app->params['phone']['primary']); ?>" title="Позвонить по телефону">
                    <span><?= $params['phone']['primary'] ?></span>
                </a>
            </div>
            <?php if ($params['phone']['secondary']) : ?>
                <div class="header__phone">
                    <a class="header__whatsapp" href="https://wa.me/<?= StringHelper::toNumber(Yii::$app->params['phone']['secondary']); ?>" target="_blank" title="Написать в Whatsapp">
                        <?= new SvgIcon('whatsapp', 'sm') ?>
                    </a>
                    <a class="link-dark header__number" href="tel:+<?= StringHelper::toNumber(Yii::$app->params['phone']['secondary']); ?>" title="Позвонить по телефону">
                        <span><?= $params['phone']['secondary'] ?></span>
                    </a>
                </div>
            <?php endif; ?>
            <a class="link-secondary header__callback" href="#" data-toggle="remote-modal" data-target="/site/api/callback-form/get">Заказать звонок</a>
        </div>

        <div class="col-xl-4 col-lg-3 col-md-4 col-sm-6 col-xs-6 col-12 header__addresses">
            <a class="link-dark header__address" href="/kontakty" title="Контакты и схема проезда"><?= $params['address']['short'] ?></a><br>
            <a class="link-secondary header__email" href="mailto:<?= $params['email']['primary'] ?>"><?= $params['email']['primary'] ?></a>
        </div>

        <div class="col-xl-2 col-lg-2 col-md-12 col-sm-6 col-12 header__rent">
            <a class="btn btn-primary header__btn" href="/arendovat" title="Оформить предварительный заказ или расчитать стоимость поездки">Арендовать</a>
            <a class="header__faq" href="/zadat-vopros">Задать вопрос</a>
        </div>

    </div><!-- /.row -->

</header>
