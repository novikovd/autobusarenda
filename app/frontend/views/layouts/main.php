<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;
use yiicom\common\base\View;

/**
 * @var View $this
 * @var string $content
 * @var array $params
 */

$this->registerAssetBundle(AppAsset::class);
$params = \Yii::$app->params;

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language; ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="yandex-verification" content="801ca8f20d3f530d">
    <meta name="google-site-verification" content="Y21EZS6jOZz99oBdpD2MVEvaUMyz2BvcrJqLwXEllqY">
    <?php echo Html::csrfMetaTags(); ?>
    <title><?php echo Html::encode($this->title); ?></title>
    <?php echo $this->head(); ?>
    <?php echo $this->render('_favicon'); ?>
</head>
<body>
<?php $this->beginBody(); ?>

    <?php echo $this->render('menu/top', ['menus' => $params['menuTop']]); ?>

    <div class="container">
        <?php echo $this->render('_header', ['params' => $params]); ?>
    </div>

    <?php echo $this->render('menu/main', ['menus' => $params['menuMain']]); ?>

    <div class="container">

        <?php echo $this->render('_breads', [
            'breads' => $this->params['breadcrumbs'] ?? []
        ]); ?>

        <div class="row">
            <div class="col-12">
                <div class="content">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>

    </div>

    <?= $this->render('menu/bottom', ['menus' => $params['menuTop']]); ?>

    <?= $this->render('_footer', ['params' => $params]); ?>

	<?= $this->render('_scripts_bottom'); ?>

<?php $this->endBody(); ?>

</body>
</html>
<?php $this->endPage(); ?>
