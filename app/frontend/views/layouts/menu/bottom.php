<?php

use yiicom\common\base\View;

/**
 * @var View $this
 * @var array $menus
 */

?>

<div class="menu menu-bottom">
    <ul class="menu__list">
        <?php foreach ($menus as $menu) : ?>
            <?php foreach ($menu['items'] as $item) : ?>
                <li class="menu__item<?= $item['link'] === $this->pathInfo ? ' active' : '' ?>">
                    <a class="menu__link" href="/<?= $item['link'] ?>"><?= $item['text'] ?></a>
                </li>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </ul>
</div>
