<?php

use yiicom\common\base\View;
use yiicom\common\helpers\SvgIcon;

/**
 * @var $this View
 * @var string $id
 * @var array $menus
 */

?>

<nav class="navbar navbar-expand-sm">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#<?= $id; ?>" aria-controls="menuMain" aria-expanded="false">
        <span class="navbar-toggler-icon">
            <?php echo new SvgIcon('menu'); ?>
        </span>
    </button>
    <div class="collapse navbar-collapse" id="<?= $id; ?>">
        <?php foreach ($menus as $menu) : ?>
            <ul class="navbar-nav <?= $menu['navClass'] ?? ''; ?>">
                <?php foreach ($menu['items'] as $item) : ?>
                    <li class="nav-item <?= isset($item['items']) ? 'dropdown' : ''  ?><?= $item['link'] === $this->pathInfo ? ' active' : '' ?>">
                        <a class="nav-link<?= isset($menu['dropdownToggle']) && isset($item['items']) ? ' dropdown-toggle' : ''  ?>" href="/<?= $item['link'] ?>">
                            <?= $item['text'] ?>
                        </a>
                        <?php if (isset($item['items'])) :  ?>
                            <ul class="dropdown-menu">
                                <?php foreach ($item['items'] as $subItem) : ?>
                                    <li class="nav-subitem">
                                        <a class="dropdown-item nav-sublink" href="/<?= $subItem['link'] ?>"><?= $subItem['text'] ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endforeach; ?>
    </div>
</nav>