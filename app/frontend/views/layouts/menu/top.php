<?php

use yiicom\common\base\View;

/**
 * @var $this View
 * @var array $menus
 */

?>

<div class="menu menu-top">
    <div class="container">
        <?= $this->render('_menu', [
            'id' => 'menuTop',
            'menus' => $menus,
        ]); ?>
    </div>
</div>
