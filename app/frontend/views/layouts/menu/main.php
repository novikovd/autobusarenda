<?php

use yiicom\common\base\View;

/**
 * @var $this View
 * @var array $menus
 */

?>

<div class="menu menu-main">
    <div class="container">
        <?= $this->render('_menu', [
            'id' => 'menuMain',
            'menus' => $menus,
        ]); ?>
    </div>
</div>
