<?php

/**
 * @var array $breads
 */

?>

<?php if ($breads) : ?>
    <div class="row">
        <div class="col-12 breads">
            <a class="bread__link" href="/">Главная</a>
            <span class="bread__sep">/</span>
            <?php foreach ($breads as $item) : ?>
                <?php if (isset($item['url'])) : ?>
                    <a class="bread__link" href="<?php echo $item['url']?>"><?php echo $item['label']?></a>
                    <span class="bread__sep">/</span>
                <?php else : ?>
                    <span class="bread__text"><?php echo $item; ?></span>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>