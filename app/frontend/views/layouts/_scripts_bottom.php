<?php

use yii\web\View;

?>

<!-- SVG --><script type="text/javascript">!function(e,t){if(t.createElementNS&&t.createElementNS("//www.w3.org/2000/svg","svg").createSVGRect)return;function n(){t.body.insertAdjacentHTML("beforeEnd",'<div style="display: none;">'+i+"</div>")}function o(){t.body?n():t.addEventListener("DOMContentLoaded",n)}var a,i,l="localStorage"in e&&null!==e.localStorage;if(l&&"123"==localStorage.getItem("inlineSVGrevision")&&(i=localStorage.getItem("inlineSVGdata")))return o();try{(a=new XMLHttpRequest).open("GET","/images/sprite.svg",!0),a.onload=function(){200<=a.status&&a.status<400&&(i=a.responseText,o(),l&&(localStorage.setItem("inlineSVGdata",i),localStorage.setItem("inlineSVGrevision","<?php echo filemtime(YS_PATH_APP . '/frontend/web/images/sprite.svg'); ?>")))},a.send()}catch(e){}}(window,document);</script><!-- /SVG -->

<!-- Modal -->
<div class="modal fade" id="modalDefault" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title"></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="spinner-border text-secondary mb-5" role="status">
                        <span class="sr-only">Загрузка...</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Modal -->

<!-- Yandex.Metrika counter -->
<?php $this->registerJs('
(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(63046840, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true });
', View::POS_END); ?>
<noscript><div><img src="https://mc.yandex.ru/watch/63046840" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->