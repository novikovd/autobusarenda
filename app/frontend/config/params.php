<?php

return [
    'menuClients' => require_once __DIR__ . '/fixture/menu_clients.php',
    'menuAbout' => require_once __DIR__ . '/fixture/menu_about.php',
    'menuContacts' => require_once __DIR__ . '/fixture/menu_contacts.php',
    'menuOrder' => require_once __DIR__ . '/fixture/menu_order.php',
    'menuTop' => require_once __DIR__ . '/fixture/menu_top.php',
    'menuMain' => require_once __DIR__ . '/fixture/menu_main.php',
];