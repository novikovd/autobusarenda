<?php

return [
    'text' => 'О нас',
    'link' => 'o-nas',
    'items' => [
        [
            'text' => 'О компании',
            'link' => 'o-nas/o-kompanii',
        ],
        [
            'text' => 'Документы',
            'link' => 'o-nas/dokumenty',
        ],
        [
            'text' => 'Новости',
            'link' => 'novosti',
        ],
        [
            'text' => 'Статьи',
            'link' => 'stati',
        ],
    ]
];