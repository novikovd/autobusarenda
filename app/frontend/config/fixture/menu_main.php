<?php

return [
    [
        'items' => [
            [
                'text' => 'Туристические автобусы',
                'link' => 'katalog-uslug/turisticheskie-avtobusy',
                'items' => [
                    ['text' => 'Экскурсионные автобусы', 'link' => 'katalog-uslug/arenda-avtobusa-dlja-jekskursij'],
                ]
            ],
            [
                'text' => 'Трансфер',
                'link' => 'katalog-uslug/transfer',
                'items' => [
                    ['text' => 'Перевозка пассажиров', 'link' => 'katalog-uslug/perevozka-passazhirov'],
                    ['text' => 'Автобус в аэропорт', 'link' => 'katalog-uslug/avtobus-v-ajeroport'],
                    ['text' => 'Трансферы на вокзал', 'link' => 'katalog-uslug/transfery-na-vokzal'],
                    ['text' => 'Междугородние автобусы', 'link' => 'katalog-uslug/mezhdugorodnie-avtobusy'],
                ]
            ],
            [
                'text' => 'Корпоративные перевозки',
                'link' => 'katalog-uslug/korporativnye-perevozki',
                'items' => [
                    ['text' => 'Автобусы на корпоратив', 'link' => 'katalog-uslug/avtobusy-na-korporativ'],
                    ['text' => 'Перевозка сотрудников', 'link' => 'katalog-uslug/perevozka-sotrudnikov'],
                    ['text' => 'Перевозка рабочих', 'link' => 'katalog-uslug/perevozka-rabochih'],
                ]
            ],
            [
                'text' => 'Автобусы на свадьбу',
                'link' => 'katalog-uslug/avtobusy-na-svadbu',
                'items' => [
                    ['text' => 'Аренда автобуса на свадьбу', 'link' => 'katalog-uslug/arenda-avtobusa-na-svadbu'],
                    ['text' => 'Заказ микроавтобуса на свадьбу', 'link' => 'katalog-uslug/zakaz-mikroavtobusa-na-svadbu'],
                ]
            ],
            [
                'text' => 'Автобусы для детей',
                'link' => 'katalog-uslug/avtobusy-dlja-detej',
                'items' => [
                    ['text' => 'Автобусы для школьников', 'link' => 'katalog-uslug/avtobusy-dlja-shkolnikov'],
                    ['text' => 'Автобус на выпускной', 'link' => 'katalog-uslug/avtobus-na-vypusknoj'],
                    ['text' => 'Аренда автобуса для экскурсий', 'link' => 'katalog-uslug/arenda-avtobusa-dlja-jekskursij'],
                    ['text' => 'Перевозка школьников', 'link' => 'katalog-uslug/perevozka-shkolnikov'],
                ]
            ],

        ]
    ],
];