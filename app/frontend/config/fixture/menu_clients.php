<?php

return [
    'text' => 'Клиентам',
    'link' => 'klientam',
    'items' => [
        [
            'text' => 'Как заказать',
            'link' => 'klientam/kak-zakazat',
        ],
        [
            'text' => 'Правила аренды',
            'link' => 'klientam/pravila-arendy',
        ],
        [
            'text' => 'Вопросы и ответы',
            'link' => 'klientam/voprosy-i-otvety',
        ],
        [
            'text' => 'Правила перевозки детей',
            'link' => 'klientam/pravila-organizovannoj-perevozki-detej',
        ],
        [
            'text' => 'Конфиденциальность',
            'link' => 'klientam/konfidencialnost',
        ],
        [
            'text' => 'Оставить отзыв',
            'link' => 'ostavit-otzyv',
        ],
    ],
];