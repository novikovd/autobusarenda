<?php

return [
    'text' => 'Арендовать',
//    'link' => 'arendovat',
    'items' => [
        [
            'text' => 'Оформить заявку',
            'link' => 'arendovat',
        ],
        [
            'text' => 'Задать вопрос',
            'link' => 'zadat-vopros',
        ],
        [
            'text' => 'Все цены',
            'link' => 'ceny',
        ],
    ]
];