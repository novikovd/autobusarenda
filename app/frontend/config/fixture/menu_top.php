<?php

$menuClients = require __DIR__ . '/menu_clients.php';
$menuAbout = require __DIR__ . '/menu_about.php';
$menuContacts = require __DIR__ . '/menu_contacts.php';

return [
    [
        'navClass' => 'mr-auto',
        'items' => [
            [
                'text' => 'Услуги и транспорт',
                'link' => 'katalog-uslug',
            ],
            [
                'text' => 'Автобусы',
                'link' => 'katalog-uslug/arenda-avtobusov'
            ],
            [
                'text' => 'Микроавтобусы',
                'link' => 'katalog-uslug/arenda-mikroavtobusov'
            ],
            [
                'text' => 'Автомобили',
                'link' => 'katalog-uslug/arenda-legkovyh-avtomobilej',
            ],
        ]
    ],
    [
        'navClass' => 'ml-auto',
        'dropdownToggle' => true,
        'items' => [
            [
                'text' => 'Цены',
                'link' => 'ceny'
            ],
            $menuAbout,
            $menuClients,
            $menuContacts
        ]
    ],


];