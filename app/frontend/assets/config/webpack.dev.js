const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./../../../common/assets/config/webpack');
const frontend = require('./webpack');

const config = merge(common, frontend, {
    mode: 'development',
    // devtool: 'cheap-module-eval-source-map',
    // devServer: {
    //     contentBase: baseConfig.externals.path.dist,
    //     port: 8081,
    //     overlay: {
    //         warnings: true,
    //         errors: true
    //     }
    // },
    // plugins: [
    //     new webpack.SourceMapDevToolPlugin({
    //         filename: '[file].map'
    //     })
    // ]
});

module.exports = new Promise((resolve, reject) => {
    resolve(config);
});