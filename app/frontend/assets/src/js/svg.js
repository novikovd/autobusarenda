/**
 * Не минимизированный код добавляющий svg спрайт на страницу
 */
(function( window, document ) {
    var file = '/images/sprite.svg';
    // var revision = '<?php echo filemtime(YS_PATH_APP . '/web/images/sprite.svg'); ?>';

    if (!!document.createElementNS && !!document.createElementNS('//www.w3.org/2000/svg', 'svg').createSVGRect) {
        return true;
    }

    var isLocalStorage = 'localStorage' in window && window[ 'localStorage' ] !== null,
        request,
        data;

    var insertIT = function() {
        document.body.insertAdjacentHTML('beforeEnd', '<div style="display: none;">' + data + '</div>');
    };

    var insert = function() {
        if (document.body) {
            insertIT();
        } else {
            document.addEventListener('DOMContentLoaded', insertIT);
        }
    };

    if (isLocalStorage && localStorage.getItem('inlineSVGrevision') == revision) {
        data = localStorage.getItem('inlineSVGdata');
        if (data) {
            insert();
            return true;
        }
    }

    try {
        request = new XMLHttpRequest();
        request.open('GET', file, true);
        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                data = request.responseText;
                insert();
                if (isLocalStorage) {
                    localStorage.setItem('inlineSVGdata', data);
                    localStorage.setItem('inlineSVGrevision', revision);
                }
            }
        };

        request.send();
    } catch(e) {}
}(window, document));