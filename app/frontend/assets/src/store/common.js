import Vue from 'vue';

export default {
    namespaced: true,

    state: {
        isLoading: false,
        errors: [],
        response: null
    },

    getters: {
        isLoading (state) {
            return state.isLoading;
        },
        errors (state) {
            return state.errors;
        },
        hasError (state) {
            return Object.keys(state.errors).length !== 0;
        }
    },

    mutations: {
        SET_LOADER (state, value) {
            state.isLoading = value;
        },
        SET_ERRORS (state, errors) {
            state.errors = errors;
        },
        SEND_SUCCESS (state, data) {
            state.response = data;
        },
    },

    actions: {
        loading ({state, commit, rootState}, value) {
            commit('SET_LOADER', value);
        },

        failing ({state, commit, rootState}, data) {
            let errors = [];

            if (typeof data.message !== 'undefined') {
                errors.push(data.message);
            } else if (Array.isArray(data)) {
                data.map(function (value) {
                    errors[value.field] = value.message;
                });
            } else {
                errors['error'] = 'Неизвестная ошибка. Пожалуйста, повторите попытку позже.';
            }

            commit('SET_ERRORS', errors);
        },

        send ({state, commit, rootState}, params) {
            return Vue.axios.post(params.url, params.data)
                .then(
                    response => commit('SEND_SUCCESS', response.data),
                    error => {}
                )
        }

    }

};
