var App = function() {
	this.$body = $('body');
    this.$document = $(document);
};

/**
* Возвращает объект jQuery
* @param {string, object} element - селектор или обект jQuery
* @returns {jQuery}
*/
App.prototype.getElement = function(element) {
   return (element instanceof jQuery) ? element : $(element);
};

/**
 * Прокручивает страницу к заданному элементу
 * @param {string|jQuery} target - элемент (объект jquery или строка селектор)
 * @param {obj} options - опции
 */
App.prototype.scrollTo = function(target, options) {
	let settings = $.extend({
            offset: 0,
            speed: 700,
            callback: false
        }, options || {});
	let position;


	if (target === 'top') {
		position = 0;
	} else {
		let $element = $(target);
		position = $element.offset().top - settings.offset;
	}

	$('html, body').animate({
		scrollTop: position
	}, {
		duration: settings.speed,
		complete: settings.callback
	});
};

/**
 * Show/hide an element on page scrolling
 * @param {string|jQuery} target Target selector or jQuery object
 * @param {int} offset Padding top
 */
App.prototype.fadeOnScroll = function(target, offset) {
    var self = this,
        $target = self.getElement(target),
        settings = {
            offset: offset || 0
        };

    self.$document.on('scroll', function() {
        if(self.$document.scrollTop() <= settings.offset) {
            $target.fadeOut();
        } else {
            $target.fadeIn();
        }
    });
};

App.prototype.initCommonEvents = function () {
    let self = this;

    // Init "scroll to" buttons
    $('[data-scroll-to]').each(function (index, element) {
        let $element = $(element);
        let target = $element.data('scroll-to');

        $element.on('click', function () {
            self.scrollTo(target);
        });
    });

    // Init modal with remote content
    $('[data-toggle="remote-modal"]').on('click', function() {
        let href = $(this).data('target');
        let $modal = $('#modalDefault');

        $modal.modal({ show: true });

        $.get(href, function (data) {
            if (data.title) {
                $('.modal-title', $modal).text(data.title);
            }

            if (data.body) {
                $('.modal-body', $modal).html(data.body);
            }
        });

        return false;
    });


};

/**
 * Inits "Goto top" button (scroll page to top)
 */
// App.prototype.initGotoTopButton = function() {
//     let self = this;
//     let $btn = $('.btn--up');
//
//     if (! $btn) {
//         return false;
//     }
//
//     self.fadeOnScroll($btn, 200);
//
//     $btn.on('click', function() {
//         self.scrollTo('top');
//
//         return false;
//     });
// };

App.prototype.init = function() {
    this.initCommonEvents();
    // this.initGotoTopButton();
};


module.exports = App;