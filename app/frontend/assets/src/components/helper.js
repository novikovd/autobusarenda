const Helper = function () {
    return {

        /**
         * Returns csrf token
         * @returns {string}
         */
        csrf: function() {
            return $('meta[name="csrf-token"]').attr('content');
        }

    };
};

module.exports = Helper;