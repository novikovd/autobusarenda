import Axios from 'axios'

const axios = Axios.create();

axios.interceptors.request.use(
    config => {
        config.headers['Content-Type'] = 'application/json';
        
        window.VueApp.$store.dispatch('common/loading', true);
        window.VueApp.$store.dispatch('common/failing', []);

        return config;
    },
    error => {
        window.VueApp.$store.dispatch('common/loading', false);
        window.VueApp.$store.dispatch('common/failing', error.response.data);

        return Promise.reject(error);
    }
);

axios.interceptors.response.use(
    response => {
        window.VueApp.$store.dispatch('common/loading', false);
        window.VueApp.$store.dispatch('common/failing', []);

        return response;
    },
    error => {
        window.VueApp.$store.dispatch('common/loading', false);
        window.VueApp.$store.dispatch('common/failing', error.response.data);

        return Promise.reject(error);
    }
);

export default axios;