const Format = function () {
    return {

        /**
         * Adds spaces between digits
         */
        asHumanNumber: function(str) {
            str = ''+str;

            return str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        },

        /**
         * Removes spaces
         */
        removeSpaces: function(str) {
            return str.replace(/\s+/g, '');
        },

    }
};

module.exports = Format;