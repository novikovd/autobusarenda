let Product = function (options) {
    this.options = $.extend({}, {
        context: '.product',
        mainImageWrap: '.product__image-wrap',
        mainImageLink: '.product__image-link',
        prevImageLink: '.product__img-link'
    }, options);

    this.$context = $(this.options.context);
    this.$mainImageWrap = $(this.options.mainImageWrap, this.$context);
    this.$mainImageLink = $(this.options.mainImageLink, this.$context);
    this.$mainImage = $('img', this.$mainImageLink);
};

Product.prototype.init = function () {
    if (! this.$context.length) {
        return false;
    }

    this.eventPreviewImageClick();
};

Product.prototype.isMediaLessThanLarge = function () {
    return this.$mainImageWrap.css('max-width') === 'none';
};

Product.prototype.eventPreviewImageClick = function () {
    let self = this;

    $(self.$context).on('click', self.options.prevImageLink, function () {
        let $link = $(this);
        let $img = $('img', $link);

        if (self.isMediaLessThanLarge()) {
            return true;
        }

        $(self.options.prevImageLink, self.$context).removeClass('selected');
        $link.addClass('selected');

        self.$mainImage
            .hide()
            .attr('src', $img.data('image'))
            .fadeIn();

        self.$mainImageLink.attr('href', $link.attr('href'));

        return false;
    });
};


module.exports = Product;