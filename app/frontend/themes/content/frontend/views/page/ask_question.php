<?php

use yii\helpers\Html;
use yiicom\common\base\View;
use yiicom\content\common\models\Page;

/**
 * @var View $this
 * @var Page $page
 */

$this->params['breadcrumbs'][] = ['label' => 'Клиентам', 'url' => '/klientam'];
$this->params['breadcrumbs'][] = Html::encode($page->name);

$this->registerJs("
    window.VueAskQuestionForm = new Vue({
        el: '#vue-ask-question-form'
    });
", View::POS_END);
?>

<div class="row">

    <div class="col-md-3">
        <?= $this->render('menu/_clients'); ?>
    </div>

    <div class="col-md-9">

        <h1><?php echo Html::encode($page->title ?: $page->name); ?></h1>

        <?php if ($page->teaser) : ?>
            <?php echo $page->teaser; ?>
        <?php endif; ?>

        <div class="row">
            <div id="vue-ask-question-form" class="col-lg-9 col-md-12">
                <ask-question-form></ask-question-form>
            </div>
        </div>

        <?php if ($page->body) : ?>
            <?php echo $page->body; ?>
        <?php endif; ?>

    </div>

</div>