<?php

use yii\web\View;
use yii\helpers\Html;
use yiicom\common\helpers\SvgIcon;
use yiicom\common\helpers\StringHelper;
use yiicom\content\common\models\Page;

/**
 * @var View $this
 * @var Page $page
 */
$this->params['breadcrumbs'][] = ['label' => 'Клиентам', 'url' => '/klientam'];
$this->params['breadcrumbs'][] = Html::encode($page->title ?: $page->name);

?>

<div class="row">

    <div class="col-md-3">
        <?= $this->render('menu/_clients'); ?>
    </div>

    <div class="col-md-9">
        <h1><?php echo Html::encode($page->title ?: $page->name); ?></h1>

        <?php if ($page->body) : ?>
            <?php echo $page->body; ?>
        <?php endif; ?>

        <p><b>Вы можете заказать автобус или микроавтобус несколькими способами</b></p>

        <p>Для просчета стоимости Ваших поездок - присылайте ваши пожелания с учетом таких данных как:</p>
        <ul class="mb-5">
            <li>место отправления и пункт назначения</li>
            <li>промежуточные точки (если необходимо)</li>
            <li>количество пассажиров</li>
            <li>модель автобуса</li>
            <li>и другие важные для вашего удобства и комфорта моменты</li>
        </ul>

        <div class="row block--row">
            <div class="col-lg-1 col-md-2 col-xs-2 block__icon text-center">
                <a href="tel:+<?= StringHelper::toNumber(\Yii::$app->params['phone']['primary']); ?>" title="Позвонить нам по телефону">
                    <?= new SvgIcon('phone', '52x52'); ?>
                </a>
            </div>
            <div class="col-lg-11 col-md-10 col-xs-10 block__desc">
                <h2 class="block__title">
                    По телефону <a class="link-dark" href="tel:+<?= StringHelper::toNumber(\Yii::$app->params['phone']['primary']); ?>" title="Позвонить по телефону"><?= \Yii::$app->params['phone']['primary'] ?></a>
                    <?php if (Yii::$app->params['phone']['secondary']) : ?>
                        <a class="link-dark" href="tel:+<?= StringHelper::toNumber(\Yii::$app->params['phone']['secondary']); ?>" title="Позвонить по телефону"><?= \Yii::$app->params['phone']['secondary'] ?></a>
                    <?php endif; ?>
                </h2>
                <div class="block__text">
                    Или Вы можете заполнить <a href="#" data-toggle="remote-modal" data-target="/site/api/callback-form/get">форму обратной связи</a> на сайте и мы вам перезвоним.
                </div>
            </div>
        </div>

        <div class="row block--row">
            <div class="col-lg-1 col-md-2 col-xs-2">
                <a class="block__icon" href="/arendovat" title="Предварительный заказ">
                    <?= new SvgIcon('form', '52x52'); ?>
                </a>
            </div>
            <div class="col-lg-11 col-md-10 col-xs-10 block__desc">
                <h2 class="block__title">
                    Заполнить <a class="link-dark" href="/arendovat">форму</a> на сайте
                </h2>
                <div class="block__text">
                    Заполните онлайн форму и наши менеджеры свяжутся с вами в ближайшее время.
                    <ul>
                        <li><a href="/arendovat">Предварительный заказ</a></li>
                        <li><a href="/zadat-vopros">Задать вопрос</a></li>
                        <li><a href="/ostavit-otzyv">Оставить отзыв</a></li>
                    </ul>

                </div>
            </div>
        </div>

        <div class="row block--row">
            <div class="col-lg-1 col-md-2 col-xs-2">
                <a class="block__icon" href="mailto:<?= \Yii::$app->params['email']['primary']; ?>" title="Напишите нам письмо">
                    <?= new SvgIcon('mail', '52x52'); ?>
                </a>
            </div>
            <div class="col-lg-11 col-md-10 col-xs-10 block__desc">
                <h2 class="block__title">
                    Написать нам письмо на <a class="link-dark" href="mailto:<?= \Yii::$app->params['email']['primary']; ?>">электронную почту</a>
                </h2>
                <div class="block__text">
                    Отправьте заявку на наш электронный почтовый ящик <a href="mailto:<?= \Yii::$app->params['email']['primary']; ?>"><?= \Yii::$app->params['email']['primary']; ?></a>
                </div>
            </div>
        </div>



    </div>

</div>