<?php

use yii\web\View;
use yiicom\common\helpers\SvgIcon;
use yiicom\content\common\models\Page;
use yiicom\common\helpers\StringHelper;

/**
 * @var View $this
 * @var Page $page
 */

$params = \Yii::$app->params;

?>

<h2 class="text-center mb-4"><a class="link-dark" href="/katalog-uslug">Наши услуги</a></h2>
<div class="row mb-5">
    <div class="col-md-4 service">
        <a class="service__image" href="/katalog-uslug/perevozka-passazhirov">
            <img class="img-fluid" src="/images/service/bus_rent_01.jpg" alt="" title="">
        </a>
        <div class="service__title"><a href="/katalog-uslug/perevozka-passazhirov">Аренда автобусов</a></div>
        <div class="service__price">от 1000 руб/час</div>
    </div>
    <div class="col-md-4 service">
        <a class="service__image" href="/katalog-uslug/arenda-mikroavtobusov">
            <img class="img-fluid" src="/images/service/microbus_rent_01.jpg" alt="" title="">
        </a>
        <div class="service__title"><a href="/katalog-uslug/arenda-mikroavtobusov">Аренда МИКРОавтобусов</a></div>
        <div class="service__price">от 800 руб/час</div>
    </div>
    <div class="col-md-4 service">
        <a class="service__image" href="/katalog-uslug/transfer">
            <img class="img-fluid" src="/images/service/transfer_03.jpg" alt="" title="">
        </a>
        <div class="service__title"><a href="/katalog-uslug/transfer">Трансфер</a></div>
        <div class="service__price">от 800 руб/час</div>
    </div>
</div>

<h2 class="text-center mb-4">Аренда автобуса с водителем</h2>
<div class="mb-5">
    <?php echo $page->teaser; ?>
</div>

<h2 class="text-center mb-4">Почему выбирают нас</h2>
<div class="row mb-5 text-center">
    <div class="col-md-4 mb-4">
        <div class="block block--present">
            <?= new SvgIcon('secure', 'xl') ?>
            <div class="block__title">Безопасность</div>
            <div class="block__text">
                Осмотр транспорта перед каждой поездкой, водители с безаварийным стажем вождения.
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-4">
        <div class="block block--present">
            <?= new SvgIcon('smile', 'xl') ?>
            <div class="block__title">Комфорт</div>
            <div class="block__text">
                Приятная поездка в чистом автобусе с удобными сидениями, большим пространством для ног и кондиционером.
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-4">
        <div class="block block--present">
            <?= new SvgIcon('clock', 'xl') ?>
            <div class="block__title">Пунктуальность</div>
            <div class="block__text">
                Гарантируем своевременное прибытие: подаем транспорт заранее, едем без задержек.
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-4">
        <div class="block block--present">
            <?= new SvgIcon('bus', 'xl') ?>
            <div class="block__title">Свой автопарк</div>
            <div class="block__text">
                Полностью отвечаем требованиям клиентов относительно класса, вместительности и комплектации транспорта.
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-4">
        <div class="block block--present">
            <?= new SvgIcon('insurance', 'xl') ?>
            <div class="block__title">Страхование пассажиров</div>
            <div class="block__text">
                Обязательное страхование пассажиров, страхование ответственности перевозчика.
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-4">
        <div class="block block--present">
            <a href="/o-nas/dokumenty"><?= new SvgIcon('license', 'xl') ?></a>
            <a class="block__title" href="/o-nas/dokumenty">Лицензия</a>
            <div class="block__text">
                Работаем на основании лицензии<br>
                № АСС-77-000260<br>
                от 07 февраля 2019 года.
            </div>
        </div>
    </div>
</div>

<h2 class="text-center mb-4">Как заказать</h2>
<div class="row mb-5">
    <div class="col-md-4 mb-4">
        <div class="block block--horizontal">
            <div class="block__header">
                <?= new SvgIcon('phone', 'lg') ?>
                <div class="block__titles">
                    <div class="block__title">По телефону</div>
                    <a class="block__sub-title" href="tel:<?= StringHelper::toNumber($params['phone']['primary']); ?>"><?= $params['phone']['primary'] ?></a>
                    <?php if ($params['phone']['secondary']) : ?>
                        <br>
                        <a class="block__sub-title" href="tel:<?= StringHelper::toNumber($params['phone']['secondary']); ?>"><?= $params['phone']['secondary'] ?></a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="block__text">
                Или заполнить <a href="#" data-toggle="remote-modal" data-target="/site/api/callback-form/get">форму</a> обратной связи на сайте и мы вам перезвоним
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-4">
        <div class="block block--horizontal">
            <div class="block__header">
                <?= new SvgIcon('form', 'lg') ?>
                <div class="block__titles">
                    <div class="block__title">Форма на сайте</div>
                    <a class="block__sub-title" href="arendovat">Онлайн форма</a>
                </div>
            </div>
            <div class="block__text">
                Заполните <a href="arendovat">онлайн форму</a> и наши менеджеры свяжутся с вами в ближайшее время
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-4">
        <div class="block block--horizontal">
            <div class="block__header">
                <?= new SvgIcon('mail', 'lg') ?>
                <div class="block__titles">
                    <div class="block__title">Электропочта</div>
                    <a class="block__sub-title" href="mailto:<?= $params['email']['primary'] ?>"><?= $params['email']['primary'] ?></a>
                </div>
            </div>
            <div class="block__text">
                Отправьте заявку на наш электронный <a class="block__sub-title" href="mailto:<?= $params['email']['primary'] ?>">почтовый ящик</a>
            </div>
        </div>
    </div>
</div>

<h2 class="text-center mb-4">Высококачественные пассажирские перевозки</h2>
<div class="mb-5">
    <?php echo $page->body; ?>
</div>