<?php

use yii\helpers\Html;
use yiicom\common\base\View;
use yiicom\content\common\models\Page;

/**
 * @var View $this
 * @var Page $page
 */

$this->params['breadcrumbs'][] = ['label' => 'Клиентам', 'url' => '/klientam'];
$this->params['breadcrumbs'][] = Html::encode($page->name);

$this->registerJs("
    window.VueReviewForm = new Vue({
        el: '#vue-review-form'
    });
", View::POS_END);
?>

<div class="row">

    <div class="col-md-3">
        <?= $this->render('menu/_clients'); ?>
    </div>

    <div class="col-md-9">

        <h1><?php echo Html::encode($page->title ?: $page->name); ?></h1>

        <?php if ($page->teaser) : ?>
            <?php echo $page->teaser; ?>
        <?php endif; ?>

        <div class="row">
            <div id="vue-review-form" class="col-lg-9 col-md-12">
                <review-form></review-form>
            </div>
        </div>

        <?php if ($page->body) : ?>
            <?php echo $page->body; ?>
        <?php endif; ?>

    </div>

</div>