<?php

use yii\web\View;
use yii\helpers\Html;
use yiicom\common\helpers\SvgIcon;
use yiicom\common\helpers\StringHelper;
use yiicom\content\common\models\Page;

/**
 * @var View $this
 * @var Page $page
 */

$this->params['breadcrumbs'][] = Html::encode($page->name);

$params = \Yii::$app->params;
?>

<div class="row">

    <div class="col-md-3">
        <?= $this->render('menu/_clients'); ?>
    </div>

    <div class="col-md-9">
        <h1><?php echo Html::encode($page->title ?: $page->name); ?></h1>

        <div class="row block--row">
            <div class="col-lg-1 col-md-2 col-xs-2 block__icon text-center">
                <a href="/o-nas/o-kompanii" title="О компании">
                    <?= new SvgIcon('company', '52x52'); ?>
                </a>
            </div>
            <div class="col-lg-11 col-md-10 col-xs-10 block__desc">
                <h2 class="block__title">
                    <a class="link-dark" href="/o-nas/o-kompanii" title="Информация о компании">О компании</a>
                </h2>
                <div class="block__text">
                    Основная специализация нашей компании - пассажирские перевозки.<br>
                    Мы предоставляем транспорт для частных и корпоративных клиентов.
                </div>
            </div>
        </div>

        <div class="row block--row">
            <div class="col-lg-1 col-md-2 col-xs-2 block__icon text-center">
                <a href="/o-nas/dokumenty" title="Документы">
                    <?= new SvgIcon('documents', '52x52'); ?>
                </a>
            </div>
            <div class="col-lg-11 col-md-10 col-xs-10 block__desc">
                <h2 class="block__title">
                    <a class="link-dark" href="/o-nas/dokumenty" title="Лицензия, свидетельство, страховка">Документы</a>
                </h2>
                <div class="block__text">
                    Лицензия, свидетельство, страховка пассажиров и другие документы
                </div>
            </div>
        </div>

        <div class="row block--row">
            <div class="col-lg-1 col-md-2 col-xs-2 block__icon text-center">
                <a href="/kontakty" title="Перейти на страницу контактов">
                    <?= new SvgIcon('contact', '52x52'); ?>
                </a>
            </div>
            <div class="col-lg-11 col-md-10 col-xs-10 block__desc">
                <h2 class="block__title">
                    <a class="link-dark" href="/kontakty" title="Контакты">Контакты</a>
                </h2>
                <div class="block__text">
                    <a class="link-dark" href="/kontakty" title="Схема проезда"><?= $params['address']['full']; ?></a><br>
                    <a href="https://wa.me/<?= StringHelper::toNumber($params['phone']['primary']); ?>" target="_blank" title="Написать в Whatsapp">
                        <?= new SvgIcon('whatsapp', 'sm') ?>
                    </a>
                    <a href="tel:+<?= StringHelper::toNumber($params['phone']['primary']); ?>" title="Позвонить по телефону">
                        <?= $params['phone']['primary'] ?>
                    </a><br>
                    <?php if ($params['phone']['secondary']) : ?>
                        <a href="https://wa.me/<?= StringHelper::toNumber($params['phone']['secondary']); ?>" target="_blank" title="Написать в Whatsapp">
                            <?= new SvgIcon('whatsapp', 'sm') ?>
                        </a>
                        <a href="tel:+<?= StringHelper::toNumber($params['phone']['secondary']); ?>" title="Позвонить по телефону">
                            <?= $params['phone']['secondary'] ?>
                        </a><br>
                    <?php endif; ?>
                    <a href="mailto:<?= $params['email']['primary'] ?>"><?= $params['email']['primary'] ?></a><br>
                </div>
            </div>
        </div>

        <div class="row block--row">
            <div class="col-lg-1 col-md-2 col-xs-2 block__icon text-center">
                <a href="/stati" title="Все статьи">
                    <?= new SvgIcon('article', '52x52'); ?>
                </a>
            </div>
            <div class="col-lg-11 col-md-10 col-xs-10 block__desc">
                <h2 class="block__title">
                    <a class="link-dark" href="/stati" title="Статьи">Статьи</a>
                </h2>
                <div class="block__text">
                    Познавательные и интересные статьи об автобусных перевозках и не только
                </div>
            </div>
        </div>

        <div class="row block--row">
            <div class="col-lg-1 col-md-2 col-xs-2 block__icon text-center">
                <a href="/novosti" title="Последние новости">
                    <?= new SvgIcon('news', '52x52'); ?>
                </a>
            </div>
            <div class="col-lg-11 col-md-10 col-xs-10 block__desc">
                <h2 class="block__title">
                    <a class="link-dark" href="/novosti" title="Новости нашей компании">Новости</a>
                </h2>
                <div class="block__text">
                    Рассказываем все самые последние новости нашей транспортной компании
                </div>
            </div>
        </div>

        <?php if ($page->body) : ?>
            <?php echo $page->body; ?>
        <?php endif; ?>
    </div>

</div>