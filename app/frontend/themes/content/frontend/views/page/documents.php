<?php

use yii\web\View;
use yii\helpers\Html;
use yiicom\content\common\models\Page;
use yiicom\common\helpers\StringHelper;

/**
 * @var View $this
 * @var Page $page
 */

$this->params['breadcrumbs'][] = ['label' => 'О нас', 'url' => '/o-nas'];
$this->params['breadcrumbs'][] = Html::encode($page->title ?: $page->name);

?>

<div class="row">

    <div class="col-md-3">
        <?= $this->render('menu/_clients'); ?>
    </div>

    <div class="col-md-9">

        <h1><?php echo Html::encode($page->title ?: $page->name); ?></h1>

        <?php if ($page->body) : ?>
            <?php echo $page->body; ?>
        <?php endif; ?>

        <h2 class="h3">Лицензия</h2>
        <div class="row mb-4">
            <div class="col-md-12">
                <a class="d-inline-block mr-2" href="/storage/uploads/documents/license_01_x.jpg" data-fancybox="gallery">
                    <img class="img-thumbnail" src="/storage/uploads/documents/license_01_m.jpg" alt="" title="">
                </a>
                <a class="d-inline-block" href="/storage/uploads/documents/license_02_x.jpg" data-fancybox="gallery">
                    <img class="img-thumbnail" src="/storage/uploads/documents/license_02_m.jpg" alt="" title="">
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <a class="d-inline-block mr-2" href="/storage/uploads/documents/license_03_x.jpg" data-fancybox="gallery">
                    <img class="img-thumbnail" src="/storage/uploads/documents/license_03_m.jpg" alt="" title="">
                </a>
                <a class="d-inline-block" href="/storage/uploads/documents/license_04_x.jpg" data-fancybox="gallery">
                    <img class="img-thumbnail" src="/storage/uploads/documents/license_04_m.jpg" alt="" title="">
                </a>
            </div>
        </div>

        <h2 class="h3">Свидетельство</h2>
        <div class="row">
            <div class="col-md-12">
                <a class="d-inline-block mr-2" href="/storage/uploads/documents/certificate_01_x.jpg" data-fancybox="gallery">
                    <img class="img-thumbnail" src="/storage/uploads/documents/certificate_01_m.jpg" alt="" title="">
                </a>
                <a class="d-inline-block" href="/storage/uploads/documents/certificate_02_x.jpg" data-fancybox="gallery">
                    <img class="img-thumbnail" src="/storage/uploads/documents/certificate_02_m.jpg" alt="" title="">
                </a>
            </div>
        </div>

    </div>

</div>