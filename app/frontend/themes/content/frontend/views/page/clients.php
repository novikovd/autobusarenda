<?php

use yii\web\View;
use yii\helpers\Html;
use yiicom\common\helpers\SvgIcon;
use yiicom\common\helpers\StringHelper;
use yiicom\content\common\models\Page;

/**
 * @var View $this
 * @var Page $page
 */

$this->params['breadcrumbs'][] = Html::encode($page->title ?: $page->name);

$params = \Yii::$app->params;
?>

<div class="row">

    <div class="col-md-3">
        <?= $this->render('menu/_clients'); ?>
    </div>

    <div class="col-md-9">

        <h1><?php echo Html::encode($page->title ?: $page->name); ?></h1>

        <div class="row block--row">
            <div class="col-lg-1 col-md-2 col-xs-2 block__icon text-center">
                <a href="/katalog-uslug" title="Каталог услуг и автотранспорта">
                    <?= new SvgIcon('bus', '52x52'); ?>
                </a>
            </div>
            <div class="col-lg-11 col-md-10 col-xs-10 block__desc">
                <h2 class="block__title">
                    <a class="link-dark" href="/katalog-uslug" title="Каталог услуг">Услуги и транспорт</a>
                </h2>
                <div class="block__text">
                    Свой автопарк полностью отвечает требованиям клиентов относительно класса, вместительности и комплектации транспорта.<br>
                    <a href="katalog-uslug" title="Каталог транспортных услуг">Каталог всех услуг и транспорта</a>
                    <ul class="mt-2">
                        <li><a href="/katalog-uslug/arenda-avtobusov">Аренда автобусов</a></li>
                        <li><a href="/katalog-uslug/arenda-mikroavtobusov">Аренда микроавтобусов</a></li>
                        <li><a href="/katalog-uslug/arenda-legkovyh-avtomobilej">Аренда легковых автомобилей</a></li>
                    </ul>
                    <a href="/ceny" title="Сравнительная таблица всех цен">Сравнительная таблица цен</a>
                </div>
            </div>
        </div>

        <div class="row block--row">
            <div class="col-lg-1 col-md-2 col-xs-2 block__icon text-center">
                <a href="/klientam/kak-zakazat" title="Способы заказа услуг">
                    <?= new SvgIcon('order', '52x52'); ?>
                </a>
            </div>
            <div class="col-lg-11 col-md-10 col-xs-10 block__desc">
                <h2 class="block__title">
                    <a class="link-dark" href="/klientam/kak-zakazat" title="Как заказать транспортные услгуи">Как заказать</a>
                </h2>
                <div class="block__text">
                    Заказать транспортные услуги можно несколькими способами:<br>
                    <ul>
                        <li>
                            Позвоните нам по телефонам:
                            <a href="tel:+<?= StringHelper::toNumber($params['phone']['primary']); ?>" title="Позвонить по телефону"><?= $params['phone']['primary'] ?></a>
                            <?php if ($params['phone']['secondary']) : ?>
                                <a href="tel:+<?= StringHelper::toNumber($params['phone']['secondary']); ?>" title="Позвонить по телефону"><?= $params['phone']['secondary'] ?></a>
                            <?php endif; ?>
                        </li>
<!--                        <li>-->
<!--                            По этим номерам телефонов доступен мессенджер-->
<!--                            <a href="https://wa.me/--><?//= StringHelper::toNumber($params['phone']['primary']); ?><!--" target="_blank" title="Написать в Whatsapp">-->
<!--                                --><?//= new SvgIcon('whatsapp', 'sm') ?><!-- WhatsApp-->
<!--                            </a>-->
<!--                        </li>-->
                        <li>
                            Заполнить <a href="#" data-toggle="remote-modal" data-target="/site/api/callback-form/get">форму обратной связи</a> и мы вам перезвоним.
                        </li>
                        <li>
                            Оформить <a href="/arendovat">заявку на сайте</a> и наш менеджер свяжется с Вами в ближайшее время.
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row block--row">
            <div class="col-lg-1 col-md-2 col-xs-2 block__icon text-center">
                <a href="/klientam/pravila-arendy" title="Правила аренды автотранспорта">
                    <?= new SvgIcon('info', '52x52'); ?>
                </a>
            </div>
            <div class="col-lg-11 col-md-10 col-xs-10 block__desc">
                <h2 class="block__title">
                    <a class="link-dark" href="/klientam/pravila-arendy" title="Правила аренды автотранспорта">Информация</a>
                </h2>
                <div class="block__text">
                    <ul>
                        <li><a href="/klientam/pravila-arendy">Правила аренды автотранспорта</a></li>
                        <li><a href="/klientam/pravila-organizovannoj-perevozki-detej">Правила организованной перевозки детей</a></li>
                        <li><a href="/klientam/konfidencialnost">Конфиденциальность</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row block--row">
            <div class="col-lg-1 col-md-2 col-xs-2 block__icon text-center">
                <a href="/klientam/voprosy-i-otvety" title="Часто задаваемые вопросы">
                    <?= new SvgIcon('faq', '52x52'); ?>
                </a>
            </div>
            <div class="col-lg-11 col-md-10 col-xs-10 block__desc">
                <h2 class="block__title">
                    <a class="link-dark" href="/klientam/voprosy-i-otvety" title="Часто задаваемые вопросы">Вопросы и ответы</a>
                </h2>
                <div class="block__text">
                    Здесь вы можете найти ответы на часто задаваемые вопросы
                    или <a href="/zadat-vopros">задать свой вопрос</a>
                </div>
            </div>
        </div>

        <?php if ($page->body) : ?>
            <?php echo $page->body; ?>
        <?php endif; ?>

    </div>

</div>