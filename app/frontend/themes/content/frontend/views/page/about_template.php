<?php

use yii\web\View;
use yii\helpers\Html;
use yiicom\content\common\models\Page;

/**
 * @var View $this
 * @var Page $page
 */
$this->params['breadcrumbs'][] = ['label' => 'О нас', 'url' => '/o-nas'];
$this->params['breadcrumbs'][] = Html::encode($page->title ?: $page->name);

?>

<div class="row">

    <div class="col-md-3">
        <?= $this->render('menu/_clients'); ?>
    </div>

    <div class="col-md-9">
        <h1><?php echo Html::encode($page->title ?: $page->name); ?></h1>

        <?php if ($page->body) : ?>
            <?php echo $page->body; ?>
        <?php endif; ?>
    </div>

</div>