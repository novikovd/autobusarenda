<?php

use yii\web\View;
use yii\helpers\Html;
use yiicom\content\common\models\Page;
use yiicom\common\helpers\StringHelper;

/**
 * @var View $this
 * @var Page $page
 */

$this->params['breadcrumbs'][] = ['label' => 'О нас', 'url' => '/o-nas'];
$this->params['breadcrumbs'][] = Html::encode($page->title ?: $page->name);

?>


<div class="row">

    <div class="col-md-3">
        <?= $this->render('menu/_clients'); ?>
    </div>

    <div class="col-md-9">
        <h1><?php echo Html::encode($page->title ?: $page->name); ?></h1>

        <h2 class="h3">Адрес</h2>
        <p><?= Yii::$app->params['address']['full']; ?></p>

        <h2 class="h3">Контакты</h2>

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3"><b>Телефон:</b></div>
                    <div class="col-md-6">
                        <a class="link" href="tel:+<?= StringHelper::toNumber(Yii::$app->params['phone']['primary']); ?>"><?= Yii::$app->params['phone']['primary']; ?></a><br>
                        <?php if (Yii::$app->params['phone']['secondary']) : ?>
                            <a class="link" href="tel:+<?= StringHelper::toNumber(Yii::$app->params['phone']['secondary']); ?>"><?= Yii::$app->params['phone']['secondary']; ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3"><b>Email:</b></div>
                    <div class="col-md-6">
                        <a class="link" href="mailto:<?= Yii::$app->params['email']['primary'] ?>"><?= Yii::$app->params['email']['primary'] ?></a>
                    </div>
                </div>
            </div>
        </div><!-- /.row -->

        <h2 class="h3">Режим работы</h2>

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3"><b>Понедельника - пятница:</b></div>
                    <div class="col-md-6">с 10:00 до 19:00</div>
                </div>
                <div class="row">
                    <div class="col-md-3"><b>Суббота - Воскресенье:</b></div>
                    <div class="col-md-6">выходной</div>
                </div>
            </div>
        </div>

        <?php if ($page->body) : ?>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $page->body; ?>
                </div>
            </div>
        <?php endif; ?>

        <h2 class="h3">Схема проезда</h2>

        <div class="row">
            <div class="col-md-12">
                <div class="ymap company__map">
                    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A0b8e686795ddbb44bf609edd2a72240207bf540f62300dd807f11a9bae56f521&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
                </div>
            </div>
        </div>

    </div>

</div>