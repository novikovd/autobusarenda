<?php

use yii\helpers\Html;
use yiicom\common\base\View;

/**
 * @var View $this
 */

$params = \Yii::$app->params;
$menus = [
    $params['menuOrder'],
    $params['menuClients'],
    $params['menuAbout'],
    $params['menuContacts'],
];

?>

<div class="sidebar">
    <?php foreach($menus as $menu) : ?>
        <nav class="sidebar__menu">
            <?php if (isset($menu['text'])) : ?>
                <h3 class="sidebar__title<?= isset($menu['link']) && $menu['link'] === $this->pathInfo ? ' active' : '' ?>">
                    <?php if (isset($menu['link'])) : ?>
                        <a href="/<?= $menu['link']; ?>"><?= Html::encode($menu['text']); ?></a>
                    <?php else : ?>
                        <?= Html::encode($menu['text']); ?>
                    <?php endif; ?>
                </h3>
            <?php endif; ?>
            <?php if (isset($menu['items']) && $menu['items']) : ?>
                <ul class="sidebar__list">
                    <?php foreach($menu['items'] as $item) : ?>
                        <li class="sidebar__item<?= $item['link'] === $this->pathInfo ? ' active' : '' ?>">
                            <a class="sidebar__link" href="/<?= Html::encode($item['link']); ?>"><?= Html::encode($item['text']); ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </nav>
    <?php endforeach; ?>
</div>