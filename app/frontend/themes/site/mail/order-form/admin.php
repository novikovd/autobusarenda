<?php

use yii\helpers\Html;
use modules\site\frontend\forms\OrderForm;
use yiicom\catalog\common\models\Product;

/**
 * @var string $subject
 * @var OrderForm $form
 * @var Product $product
 */

?>
<html>
<head>
    <title>Форма "<?= Html::encode($subject); ?>"</title>
</head>
<body>
    <b>Имя:</b> <?= Html::encode($form->name); ?><br />
    <b>Телефон:</b> <?= Html::encode($form->phone); ?><br />
    <b>Email:</b> <?= Html::encode($form->email); ?><br />
    <b>Сообщение:</b> <?= Html::encode($form->comment); ?><br />

    <?php if ($product) : ?>
        <b>Тип транспорта:</b> <?= Html::a($product->name, "@frontendWeb/{$product->url->alias}"); ?><br />
    <?php endif; ?>

    <?php if ($form->type) : ?>
        <b>Тип поездки:</b> <?= $form->types()[$form->type] ?? 'нет'; ?><br />
    <?php endif; ?>

    <?php if ($form->start) : ?>
        <b>Начало аренды:</b> <?= Html::encode($form->start); ?><br />
    <?php endif; ?>

    <?php if ($form->end) : ?>
        <b>Окончание аренды:</b> <?= Html::encode($form->end); ?><br />
    <?php endif; ?>

    <?php if ($form->from) : ?>
        <b>Адрес подачи:</b> <?= Html::encode($form->from); ?><br />
    <?php endif; ?>

    <?php if ($form->to) : ?>
        <b>Адрес назначение:</b> <?= Html::encode($form->to); ?><br />
    <?php endif; ?>
</body>
</html>