<?php

use yii\helpers\Html;
use modules\site\frontend\forms\ReviewForm;

/**
 * @var string $subject
 * @var ReviewForm $form
 */

?>
<html>
<head>
    <title>Форма "<?= Html::encode($subject); ?>"</title>
</head>
<body>
    <b>Имя:</b> <?= Html::encode($form->name); ?><br />
    <b>Телефон:</b> <?= Html::encode($form->phone); ?><br />
    <b>Email:</b> <?= Html::encode($form->email); ?><br />
    <b>Заголовок:</b> <?= Html::encode($form->title); ?><br />
    <b>Компания:</b> <?= Html::encode($form->company); ?><br />
    <b>Оценка:</b> <?= Html::encode($form->rating); ?><br />
    <b>Отзыв:</b> <?= Html::encode($form->comment); ?><br />
</body>
</html>