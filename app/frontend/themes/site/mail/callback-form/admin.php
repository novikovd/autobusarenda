<?php

use yii\helpers\Html;
use modules\site\frontend\forms\CallbackForm;

/**
 * @var string $subject
 * @var CallbackForm $form
 */

?>
<html>
<head>
    <title>Форма "<?= Html::encode($subject); ?>"</title>
</head>
<body>
    <b>Имя:</b> <?= Html::encode($form->name); ?><br />
    <b>Телефон:</b> <?= Html::encode($form->phone); ?><br />
    <b>Коментарий:</b> <?= Html::encode($form->comment); ?><br />
</body>
</html>