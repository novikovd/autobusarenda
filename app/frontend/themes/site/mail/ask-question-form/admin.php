<?php

use yii\helpers\Html;
use modules\site\frontend\forms\AskQuestionForm;

/**
 * @var string $subject
 * @var AskQuestionForm $form
 */

?>
<html>
<head>
    <title>Форма "<?= Html::encode($subject); ?>"</title>
</head>
<body>
    <b>Имя:</b> <?= Html::encode($form->name); ?><br />
    <b>Телефон:</b> <?= Html::encode($form->phone); ?><br />
    <b>Email:</b> <?= Html::encode($form->email); ?><br />
    <b>Тема:</b> <?= Html::encode($form->category); ?><br />
    <b>Вопрос:</b> <?= Html::encode($form->question); ?><br />
</body>
</html>