<?php
?>

<div id="vue-callback-form">
    <callback-form></callback-form>
</div>

<script>
    window.VueCallbackForm = new Vue({
        el: '#vue-callback-form'
    });
</script>