<?php

use yii\web\View;
use yii\helpers\Html;
use yiicom\content\common\models\Page;

/**
 * @var View $this
 * @var Page $page
 * @var array $categories
 * @var array $products
 */

$this->params['breadcrumbs'][] = ['label' => 'Клиентам', 'url' => '/klientam'];
$this->params['breadcrumbs'][] = Html::encode($page->name);

?>

<div class="row">

    <div class="col-md-3">
        <?= $this->render('@yiicom/content/frontend/views/page/menu/_clients'); ?>
    </div>

    <div class="col-md-9">

        <h1><?php echo Html::encode($page->title ?: $page->name); ?></h1>

        <?php echo $page->body; ?>

        <?php foreach ($categories as $category) : ?>

            <?php if (! (isset($products[$category->id]) && $products[$category->id])) : ?>
                <?php continue; ?>
            <?php endif; ?>

            <h3 class="mb-4"><a href="/<?= $category->url->alias ?>"><?= Html::encode($category->name); ?></a></h3>

            <table class="table table-striped table-bordered table-hover mb-5">
                <thead>
                    <tr>
                        <th>Модель</th>
                        <th>Стоимость руб/час</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($products[$category->id] as $product) : ?>
                        <tr>
                            <td><a class="link-gray" href="/<?= $product->url->alias; ?>" title="Подробнее"><?= Html::encode($product->name); ?></a></td>
                            <td><?= $product->price ?: ''; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

        <?php endforeach; ?>

    </div>

</div>