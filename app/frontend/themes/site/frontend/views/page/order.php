<?php

use yii\helpers\Html;
use yiicom\common\base\View;
use yiicom\content\common\models\Page;
use yiicom\catalog\common\models\Product;

/**
 * @var View $this
 * @var Page $page
 * @var Product|null $product
 */

$this->params['breadcrumbs'][] = ['label' => 'Клиентам', 'url' => '/klientam'];
$this->params['breadcrumbs'][] = Html::encode($page->name);

$this->registerJs("
    window.VueOrderForm = new Vue({
        el: '#vue-order-form'
    });
", View::POS_END);
?>

<div class="row">

    <div class="col-md-3">
        <?= $this->render('@yiicom/content/frontend/views/page/menu/_clients'); ?>
    </div>

    <div class="col-md-9">

        <h1><?php echo Html::encode($page->title ?: $page->name); ?></h1>

        <?php if ($page->teaser) : ?>
            <?php echo $page->teaser; ?>
        <?php endif; ?>

        <div class="row">
            <?php if ($product) : ?>
                <div class="col-md-12 mb-4">
                    Вид транспорта: <a href="/<?= $product->url->alias; ?>"><?= Html::encode($product->name); ?></a>
                </div>
            <?php endif; ?>

            <div id="vue-order-form" class="col-lg-9 col-md-12">
                <order-form :product="<?= $product->id ?? 0; ?>"></order-form>
            </div>
        </div>

        <?php if ($page->body) : ?>
            <?php echo $page->body; ?>
        <?php endif; ?>

    </div>

</div>
