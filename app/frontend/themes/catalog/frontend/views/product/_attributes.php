<?php

use yii\helpers\Html;
use yiicom\catalog\common\models\AttributeType;
use yiicom\catalog\common\models\Product;

/**
 * @var array $attributes
 * @var Product $product
 * @var bool|null $isShowInProduct
 * @var bool|null $isShowInCard
 */

?>

<ul class="attributes">
    <?php foreach ($attributes as $attributeGroup) : ?>
        <?php if (isset($attributeGroup['attributes'])) : ?>
            <?php foreach ($attributeGroup['attributes'] as $attribute) : ?>
                <?php if (isset($product->attributeValue->value[$attribute['id']])) : ?>

                    <?php if (isset($isShowInProduct) && $isShowInProduct && ! $attribute['isShowInProduct']) : ?>
                        <?php continue; ?>
                    <?php endif; ?>

                    <?php if (isset($isShowInCard) && $isShowInCard && ! $attribute['isShowInCard']) : ?>
                        <?php continue; ?>
                    <?php endif; ?>

                    <li class="attribute">
                        <?php if ($attribute['type'] == AttributeType::TYPE_CHECKBOX) : ?>
                            <span><?= Html::encode($attribute['title']) ?></span>: есть
                        <?php elseif ($attributeGroup['type'] == AttributeType::TYPE_SELECT) : ?>
                            <span><?= Html::encode($attributeGroup['title']) ?></span>: <?= Html::encode($attribute['title']) ?>
                        <?php else : ?>
                            <span><?= Html::encode($attribute['title']) ?></span>:
                            <?= isset($product->attributeValue->value[$attribute['id']]) ? Html::encode($product->attributeValue->value[$attribute['id']]) : '' ?>
                        <?php endif; ?>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>