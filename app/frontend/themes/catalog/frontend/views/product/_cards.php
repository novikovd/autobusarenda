<?php

use yii\helpers\Html;
use yiicom\files\common\widgets\ImageWidget;
use yiicom\catalog\common\models\Product;

/**
 * @var Product[] $products
 * @var array $attributes
 */

$count = count($products);
?>

<?php foreach ($products as $product) : ?>

    <div class="col-md-<?= 12 / $count ?> col-sm-6 col-xs-12 text-sm-left text-xs-center relation-card">

        <a class="relation-card__title" href="/<?= $product->url->alias ?>"><?= Html::encode($product->name) ?></a>

        <div class="row relation-card__desc">

            <div class="col-xs-12 mb-3">
                <a class="relation-card__image" href="/<?= $product->url->alias ?>">
                    <?php if (isset($product->files[0])) : ?>
                        <?php echo ImageWidget::widget([
                            'images' => $product->files[0],
                            'options' => ['class' => 'img-fluid relation-card__img'],
                            'preset' => 'card',
                        ]); ?>
                    <?php endif; ?>
                </a>
            </div>

            <div class="col-xs-12">
                <?= $this->render('_attributes', [
                    'product' => $product,
                    'attributes' => $attributes,
                    'isShowInCard' => true,
                ]); ?>

                <a class="relation-card__link" href="/<?= $product->url->alias ?>">Подробнее</a>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-12 mb-3">
                <?php if ($product->isShowPrice) : ?>
                    <div class="relation-card__price">от <span><?= $product->price ?></span> руб/час</div>
                <?php endif; ?>
            </div>

            <div class="col-xs-12">
                <a class="btn btn-primary btn-lg relation-card__btn-rent" href="/arendovat?productId=<?= $product->id; ?>"
                   title="Оформить предварительный заказ или расчитать стоимость поездки">Арендовать</a>
            </div>

        </div>

    </div>

<?php endforeach; ?>
