const path = require('path');
const webpack = require('webpack');
const { VueLoaderPlugin } = require('vue-loader');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');

module.exports = {
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    name: 'vendors',
                    test: /node_modules/,
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env'], // es2015
                    plugins: ['@babel/plugin-transform-runtime']
                }
            }
        }, {
            test: /\.vue$/,
            loader: 'vue-loader',
            options: {
                loader: {
                    scss: 'vue-style-loader!css-loader!sass-loader'
                }
            }
        }, {
            test: /\.scss$/,
            use: [
                'style-loader',
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: { sourceMap: true }
                },{
                    loader: 'postcss-loader',
                    options: {
                        sourceMap: true,
                        config: { path: './app/common/assets/config/postcss.config.js'}
                    }
                },
                {
                    loader: 'resolve-url-loader'
                },
                {
                    loader: 'sass-loader',
                    options: { sourceMap: true }
                }
            ]
        }, {
            test: /\.svg$/,
            use: [
                {
                    loader: 'svg-sprite-loader',
                    options: {
                        extract: true,
                        spriteFilename: './../../web/images/sprite.svg',
                        symbolId: filePath => {
                            let name = path.basename(filePath, '.svg');
                            return `icon-${name.replace(/_/g, '-')}`
                        },
                    }
                },
                // 'svg-transform-loader',
                'svgo-loader'
            ]
        },
            {
            test: /\.(jpg|png|gif)$/,
            use: {
                loader: 'url-loader',
                options: {
                    limit: 1,
                    name: "[name].[ext]",
                    outputPath: "images"
                }
            }
        }
        ]
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.common.js'
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': "jquery"
        }),
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: "css/[name].css",
        }),
        new SpriteLoaderPlugin({
            plainSprite: true
        }),
        {
            apply: (compiler) => {
                compiler.plugin('emit', (compilation, callback) => {
                    delete compilation.assets['js/sprite.js'];
                    callback();
                });
            }
        }
    ]
};
