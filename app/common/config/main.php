<?php

return [
    'id' => 'autobusarenda.ru',
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-US',
    'charset' => 'utf-8',
    'extensions' => require(YS_PATH_VENDOR . '/yiisoft/extensions.php'),
    'vendorPath' => YS_PATH_VENDOR,
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'bootstrap' => [
        'log'
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => getenv('DB_DSN'),
            'username' => getenv('DB_USER'),
            'password' => getenv('DB_PASSWORD'),
            'charset' => 'utf8',
        ],
        'i18n' => [
            // TODO: move to modules
            'translations' => [
                'yiicom' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@yiicom/common/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'yiicom/content' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@yiicom/content/common/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'yiicom/catalog' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@yiicom/catalog/common/messages',
                    'sourceLanguage' => 'en-US',
                ],
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'formatter' => [
            'dateFormat' => 'php:d.m.Y',
        ],
        'session' => [
            'class' => \yii\web\Session::class,
            'cookieParams' => [
                'lifetime' => 7 * 24 * 60 * 60
            ]
        ],
        'mailer' => [
            'class' => \yii\swiftmailer\Mailer::class,
            'useFileTransport' => false,
            'viewPath' => '@common/mail',
            'messageConfig' => [
                'from' => [getenv('MAIL_FROM') => getenv('MAIL_NAME')],
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'username' => getenv('MAIL_FROM'),
                'password' => getenv('MAIL_PASSWORD'),
                'host' => getenv('MAIL_HOST'),
                'port' => getenv('MAIL_POST'),
            ],
        ],
    ],
    'modules' => [

    ],
    'params' => [
        'phone' => [
            'primary' => '+7 (495) 130-02-23',
            'secondary' => '',
        ],
        'email' => [
            'primary' => 'info@autobusarenda.ru',
        ],
        'address' => [
            'full' => 'г. Москва, ул. Искры, д. 31, корп. 1, Бизнес центр "Искра"',
            'middle' => 'г. Москва, ул. Искры, д. 31к1, БЦ "Искра"',
            'short' => 'г. Москва, ул. Искры, д. 31к1',
        ],
        'company' => [
            'title' => 'ИП Новикова Н. Ю.',
            'inn' => '770100042417'
        ],
        'pagination' => [
            'pageSize' => 20,
        ]
    ],
];